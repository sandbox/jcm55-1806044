<?php
/**
 *  %%%copyright%%%
 *                            %%%copyright%%%
 *
 * FusionTicket - ticket reservation system
 *    Copyright (C) 2007-2012 Christopher Jenkins, Niels, Lou. All rights reserved.
 *
 * Original Design:
 *    phpMyTicket - ticket reservation system
 *    Copyright (C) 2004-2005 Anna Putrino, Stanislav Chachkov. All rights reserved.
 *
 * This file is part of FusionTicket.
 *
 * This file may be distributed and/or modified under the terms of the "GNU General
 * Public License" version 3 as published by the Free Software Foundation and appearing
 * in the file LICENSE included in the packaging of this file.
 *
 * This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY
 * OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Any links or references to Fusion Ticket must be left in under our licensing agreement.
 *
 * By USING this file you are agreeing to the above terms of use. REMOVING this licence
 * does NOT remove your obligation to the terms of use.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * Contact help@fusionticket.com if any conditions of this licencing isn't clear to you.
 */

// Create on: Mon, 17 Sep 2012 10:43:08 -0700

// The following settings are automatically filled by the installation procedure:

define("CURRENT_VERSION","Beta7.0b");

$_SHOP->db_host  =  'localhost';
$_SHOP->db_name  =  'fusionticket';
$_SHOP->db_pass  =  'xxxxxxx';
$_SHOP->db_uname  =  'actarts';
$_SHOP->secure_id  =  'd6f5b67c84b4aca3b4d896ab72df3ba3e71b3d76';

?>
