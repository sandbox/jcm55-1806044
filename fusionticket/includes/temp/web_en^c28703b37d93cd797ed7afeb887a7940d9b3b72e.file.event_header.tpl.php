<?php /* Smarty version Smarty-3.1-DEV, created on 2012-09-26 22:34:40
         compiled from "/home/actarts/test/db/sites/all/modules/civievent_fusionticket/fusionticket/includes/template/web/event_header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7413278035063e570257c09-69859381%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c28703b37d93cd797ed7afeb887a7940d9b3b72e' => 
    array (
      0 => '/home/actarts/test/db/sites/all/modules/civievent_fusionticket/fusionticket/includes/template/web/event_header.tpl',
      1 => 1348543750,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7413278035063e570257c09-69859381',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'shop_event' => 0,
    '_SHOP_themeimages' => 0,
    'info_plus' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5063e570412304_71911247',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5063e570412304_71911247')) {function content_5063e570412304_71911247($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/actarts/test/db/sites/all/modules/civievent_fusionticket/fusionticket/includes/libs/smarty3/plugins/modifier.date_format.php';
?><!-- $Id: event_header.tpl 1846 2012-08-06 17:12:58Z nielsNL $ -->
                                <div class="art-content-layout-br layout-item-0"></div>
                                <div class="art-content-layout layout-item-1">
                                  <div class="art-content-layout-row">
                                    <div class="art-layout-cell layout-item-2" style="width: 30%;">
                                      <?php echo $_smarty_tpl->smarty->registered_objects['gui'][0]->image(array('href'=>((string)$_smarty_tpl->tpl_vars['shop_event']->value['event_image']),'width'=>160,'height'=>150,'align'=>'left','class'=>"magnify has-tooltip",'border'=>"0",'style'=>'','alt'=>((string)$_smarty_tpl->tpl_vars['shop_event']->value['event_name'])." in ".((string)$_smarty_tpl->tpl_vars['shop_event']->value['ort_city']),'title'=>((string)$_smarty_tpl->tpl_vars['shop_event']->value['event_name'])." in ".((string)$_smarty_tpl->tpl_vars['shop_event']->value['ort_city'])),$_smarty_tpl);?>

                                    </div>
                                    <div class="art-layout-cell layout-item-3" style="width: 70%;">
                                      <ul>
                                        <li><b><?php echo con("event_name");?>
:</b>
                                          <a class="title_link" href='<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['shop_event']->value['event_id'];?>
<?php $_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->_Url(array('file'=>'index.php','event_id'=>$_tmp1),$_smarty_tpl);?>
'>
                                            <?php echo $_smarty_tpl->tpl_vars['shop_event']->value['event_name'];?>

                                          </a>
                                          <?php if ($_smarty_tpl->tpl_vars['shop_event']->value['event_mp3']){?>
                                            <a  href='files/<?php echo $_smarty_tpl->tpl_vars['shop_event']->value['event_mp3'];?>
'>
                                              <img src='<?php echo $_smarty_tpl->tpl_vars['_SHOP_themeimages']->value;?>
audio-small.png' border='0' valign='bottom'>
                                            </a>
                                          <?php }?>
                                        </li>
                                        <li>
                                           <b><?php echo con("date");?>
:</b> <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['shop_event']->value['event_date'],con("shortdate_format"));?>
 - <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['shop_event']->value['event_time'],con("time_format"));?>

                                        </li>
                                        <?php if ($_smarty_tpl->tpl_vars['info_plus']->value&&$_smarty_tpl->tpl_vars['shop_event']->value['event_open']){?>
                                          <li><b><?php echo con("doors_open");?>
</b> <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['shop_event']->value['event_open'],con("time_format"));?>
</li>
                                        <?php }?>
                                        <li>
                                          <b><?php echo con("venue");?>
:</b>
                                          <a onclick='showDialog(this);return false;' href='address.php?event_id=<?php echo $_smarty_tpl->tpl_vars['shop_event']->value['event_id'];?>
'><?php echo $_smarty_tpl->tpl_vars['shop_event']->value['ort_name'];?>
</a> -
                                          <?php echo $_smarty_tpl->tpl_vars['shop_event']->value['ort_city'];?>
 - <?php echo $_smarty_tpl->tpl_vars['shop_event']->value['pm_name'];?>

                                        </li>
                                      </ul>
                                      <?php if ($_smarty_tpl->tpl_vars['shop_event']->value['event_short_text']){?>
                                      <blockquote style="margin: 10px 0"><?php echo $_smarty_tpl->tpl_vars['shop_event']->value['event_short_text'];?>
</blockquote>
                                      <?php }?>
                                    </div>
                                  </div>
                                </div><?php }} ?>