<?php
if (!defined("ft_check")) { die("System intrusion"); }

class Smartyplugincache {
  function __call($name, $args) { return ''; }

  function __initialize($smarty) {
        $plugs = new Smartyplugincache();
        $smarty->registerobject('plugin', $plugs, null, true, null); }
}
