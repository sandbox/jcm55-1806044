<?php /* Smarty version Smarty-3.1-DEV, created on 2012-09-26 22:35:42
         compiled from "/home/actarts/test/db/sites/all/modules/civievent_fusionticket/fusionticket/includes/template/web/event_description.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2924041965063e5ae1311b4-99143763%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd52cdb291517f60beb6e775f068dae26b2c255a8' => 
    array (
      0 => '/home/actarts/test/db/sites/all/modules/civievent_fusionticket/fusionticket/includes/template/web/event_description.tpl',
      1 => 1348543750,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2924041965063e5ae1311b4-99143763',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'shop_event' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5063e5ae2845a5_52139278',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5063e5ae2845a5_52139278')) {function content_5063e5ae2845a5_52139278($_smarty_tpl) {?><?php if (!is_callable('smarty_block_event')) include '/home/actarts/test/db/sites/all/modules/civievent_fusionticket/fusionticket/includes/shop_plugins/block.event.php';
if (!is_callable('smarty_modifier_date_format')) include '/home/actarts/test/db/sites/all/modules/civievent_fusionticket/fusionticket/includes/libs/smarty3/plugins/modifier.date_format.php';
?><!-- $Id: event_description.tpl 1822 2012-07-11 22:26:35Z nielsNL $ -->
  <?php if ($_smarty_tpl->tpl_vars['shop_event']->value['event_text']){?>
    <div class="art-content-layout-br layout-item-0"></div>
    <b><?php echo con("event_description");?>
</b><br>
    <div class="art-content-layout">
      <div class="art-content-layout-row">
        <div class="art-layout-cell layout-item-4" style="width: 100%;">
           <?php echo $_smarty_tpl->tpl_vars['shop_event']->value['event_text'];?>

        </div>
      </div>
    </div>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['shop_event']->value['event_rep']=='main'){?>
    <div class="art-content-layout-br layout-item-0"></div>
    <b><?php echo con("dates_localities");?>
</b><br>
    <div class="art-content-layout">
      <div class="art-content-layout-row">
        <div class="art-layout-cell layout-item-4" style="width: 100%;"><ul>
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('event', array('event_main_id'=>$_smarty_tpl->tpl_vars['shop_event']->value['event_id'],'ort'=>'on','stats'=>'on','sub'=>'on','event_status'=>'pub','place_map'=>'on','order'=>"event_date,event_time")); $_block_repeat=true; echo smarty_block_event(array('event_main_id'=>$_smarty_tpl->tpl_vars['shop_event']->value['event_id'],'ort'=>'on','stats'=>'on','sub'=>'on','event_status'=>'pub','place_map'=>'on','order'=>"event_date,event_time"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <li>
              <a href="index.php?event_id=<?php echo $_smarty_tpl->tpl_vars['shop_event']->value['event_id'];?>
">
                <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['shop_event']->value['event_date'],con("date_format"));?>

              </a>
	            <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['shop_event']->value['event_time'],con("time_format"));?>
 <?php echo $_smarty_tpl->tpl_vars['shop_event']->value['pm_name'];?>

            </li>
          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_event(array('event_main_id'=>$_smarty_tpl->tpl_vars['shop_event']->value['event_id'],'ort'=>'on','stats'=>'on','sub'=>'on','event_status'=>'pub','place_map'=>'on','order'=>"event_date,event_time"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          </ul>
          <?php if (!$_smarty_tpl->tpl_vars['shop_event']->value['event_main_id']){?>
          	  <div class="art-content-layout-br layout-item-0"></div>
              <div class="art-content-layout layout-item-1">
                <div class="art-content-layout-row" style='padding:10px;'>
                  <p><center><?php echo con("no_sub_events");?>
</center></p>
               </div>
              </div>
          <?php }?>
        </div>
      </div>
    </div>

<?php }else{ ?>
	<?php if (!$_smarty_tpl->tpl_vars['shop_event']->value['event_pm_id']){?>
	  <div class="art-content-layout-br layout-item-0"></div>
    <div class="art-content-layout layout-item-1">
      <div class="art-content-layout-row" style='padding:10px;'>
        <p><center><?php echo con("no_placemap_available");?>
</center></p>
      </div>
    </div>
	<?php }elseif($_smarty_tpl->tpl_vars['shop_event']->value['category_web']>=1){?>
		<?php echo $_smarty_tpl->getSubTemplate ("event_prices.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

  <?php }else{ ?>
	  <div class="art-content-layout-br layout-item-0"></div>
    <div class="art-content-layout layout-item-1">
      <div class="art-content-layout-row" style='padding:10px;'>
    <p><center><?php echo con("no_categories_available");?>
</center></p>
     </div>
    </div>
  <?php }?>
<?php }?><?php }} ?>